import click

from clients import commands as clients_commands
#del modulo clients importa commands como si se llamarar clients_commands
CLIENTS_TABLE = '.clients.csv'

@click.group()
@click.pass_context #Nos da un objeto contexto
def cli(ctx):
    ctx.obj = {}
    ctx.obj['clients_table'] = CLIENTS_TABLE

cli.add_command(clients_commands.all)
    