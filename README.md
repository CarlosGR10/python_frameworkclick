Este es un proyecto donde realizamos un CRUD en python con el framework Click

Para poder ejecutar el programa necesitamos un entorno vitual

>>> virtualenv --python=python3 venv

Para crear el ambiente virtual es :

>>> source venv/bin/activate

si ya ejecutamos el entorno virtual 

>>> pip install --editable .

Una vez instalado ya podemos trabajar 

ejemplo.

pv clients --help

pv clients list

pv clients (cualquier comando)