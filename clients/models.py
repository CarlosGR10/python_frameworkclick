import uuid #Nos permite crear uid unicas, que es el estandard 

class Client:

    def __init__(self, name, company, email, position , uid=None):
        self.name = name
        self.company = company
        self.email = email
        self.position = position
        self.uid = uid or uuid.uuid4() #Si no recibe como parameto la variable uid , devuelve el id UUID4, que es el estandard

    def to_dict(self):
        return vars(self) #LA funcion global "vars" , realiza un diccionario a nuetro objeto, chequea lo que regresa el metodo 

    @staticmethod # Nos permite ejecutar metodos estaticos(Se puede ejecutar sin una estancia de clase) 
    def schema(): #Es la representacion de un esquema columnal
        return ['name', 'company', 'email', 'position', 'uid']
